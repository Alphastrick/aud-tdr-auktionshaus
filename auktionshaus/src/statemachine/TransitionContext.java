package statemachine;

/**
 * TransitionContext which contains rt information of a tranistion.
 * The TransitionContext is passed to the transitioning action.
 */
public class TransitionContext {
    
    private final Transition transition;
    private boolean isBlocked = false;

    /**
     * TransitionContext
     * @param transition Transition (Context transition)
     */
    TransitionContext(Transition transition) {
        this.transition = transition;
    }

    /**
     * Block transition
     */
    public void blockTransition() {
        this.isBlocked = true;
    }

    /**
     * Returns if the transition is blocked
     * @return True if the transition is blocked
     */
    public boolean isTransitionBlocked() {
        return this.isBlocked;
    }

}
