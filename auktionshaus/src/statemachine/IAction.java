package statemachine;

public interface IAction {
    
    String id();
    void performIn(TransitionContext context);

}
