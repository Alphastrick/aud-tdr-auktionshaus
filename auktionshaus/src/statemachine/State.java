package statemachine;

public class State {
 
    public final String id;

    /**
     * StateMachine State
     * @param id State id
     */
    public State(String id) {
        this.id = id;
    }

}
