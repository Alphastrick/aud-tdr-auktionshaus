package statemachine;

import java.util.*;

public class StateMachine {

    /**
     * Exception
     */
    public class FailedTransition extends Exception {

        public FailedTransition(String message) {
            super(message);
        }
        
        public FailedTransition(String message, Throwable cause) {
            super(message, cause);
        }
       
        public FailedTransition(String message,
                                Throwable cause,
                                boolean enableSuppression,
                                boolean writableStackTrace) {
            super(message, cause);
        }

    }

    private final HashMap<String, Transition> transitions;
    private State state;

    /**
     * StateMachine
     * 
     * @param initState Inital state of the state machine
     * @param transitions Map of available transitions
     */
    public StateMachine(State initState, HashMap<String, Transition> transitions) {
        
        this.state = initState;
        this.transitions = transitions;

    }

    /**
     * Dispatch given action in order to switch state
     * @param action Action to dispatch
     * @throws statemachine.StateMachine.FailedTransition if transition fails
     *         or is blocked
     */
    public void dispatch(IAction action) throws FailedTransition {

        Transition transition = this.transitions.get(action.id());
        
        if (transition == null) {
            throw new FailedTransition("Transition was not found");
        } 

        TransitionContext context = new TransitionContext(transition);

        action.performIn(context);
    
        if (context.isTransitionBlocked()) {
            throw new FailedTransition("Transition was blocked by the action");
        }

        this.state = transition.targetState;

    }

    /**
     * Get state machine state
     * @return State machine state
     */
    public State getState() {

        return this.state;

    }

}
