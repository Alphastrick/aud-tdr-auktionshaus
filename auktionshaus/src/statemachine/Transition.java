package statemachine;

/**
 * Transition associates two states to a possible state transition.
 */
public class Transition {
    
    public final State sourceState;
    public final State targetState;

    /**
     * Transition
     * @param source Source-State to transit from
     * @param target Target-State to transit to
     */
    public Transition(State source, State target) {

        this.sourceState = source;
        this.targetState = target;

    }

}
