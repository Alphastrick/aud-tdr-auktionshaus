package auktionshaus;

public class Product implements IDataItem {
    private int id = -1;
    private String name;
    private String description;
    private byte[] image;
    private float priceEstimation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public float getPriceEstimation() {
        return priceEstimation;
    }

    public void setPriceEstimation(float priceEstimation) {
        this.priceEstimation = priceEstimation;
    }
    
    @Override
    public void acceptVisitor(IDataVisitor visitor) {
        visitor.visitProduct(this);
    }
    
    @Override
    public boolean equals(Object o) {
        
        if ((o instanceof Product)) {
            
            Product p = (Product)o;
            return (p.id == this.id || p.id == 0 || this.id == 0) && p.name == this.name;
            
        }
        
        return false;
    }
    
}
