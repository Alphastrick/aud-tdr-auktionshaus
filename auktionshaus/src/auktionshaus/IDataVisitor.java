package auktionshaus;

public interface IDataVisitor {
 
    void visitAuction(Auction auction);
    void visitProduct(Product product);
    
}
