package auktionshaus;

public interface IDataItem {
    
    void acceptVisitor(IDataVisitor visitor);
    
}
