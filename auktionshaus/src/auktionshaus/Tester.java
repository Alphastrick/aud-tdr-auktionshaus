package auktionshaus;

import auktionshaus.Persistence.AuctionSqlAdapter;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import auktionshaus.Persistence.KoppelSqlAdapter;
import auktionshaus.Persistence.ProductSqlAdapter;
import auktionshaus.Persistence.SqlAdapterFactory;
import auktionshaus.Persistence.SqlReadStrategyFactory;
import java.time.LocalDateTime;
import java.util.List;

public class Tester {
    private SqlAdapterFactory factory;
    private SqlReadStrategyFactory readfactory;
    
    public Tester(String user, String pw){
        factory = new SqlAdapterFactory(user,pw);
        readfactory = new SqlReadStrategyFactory();
    }
    public boolean testProduct(){
        try{
           //test read all
           ProductSqlAdapter adapter = (ProductSqlAdapter) factory.createProductAdapter();
           List<Product> products = adapter.read((ISqlReadStrategy) readfactory.createAllProductReadStrategy());
           //test add
           Product product = new Product();
           product.setDescription("Test");
           product.setName("Product" + products.size());
           product.setPriceEstimation(0);
           byte[] image = {100};
           product.setImage(image);
           adapter.insert(product);
           //test update 
           product.setId(products.size());
           product.setName("Replacement");
           adapter.update(product);
           //add Trenner
           product.setDescription("Trenner");
           product.setName("Trenner");
           adapter.insert(product);
           product.setId(products.size() + 1);
           //test delete
           adapter.delete(product);
           //test read single
           products = adapter.read((ISqlReadStrategy) readfactory.createSingleProductReadStrategy(products.size()));
           products.forEach(i -> {
                System.out.println(i.getName());
           });
           return true;
        }
        catch(Exception e){
            return false;
        }
    }
    public boolean testAuction(){
        try{
            //test read all
           AuctionSqlAdapter adapter = (AuctionSqlAdapter) factory.createAuctionAdapter();
           List<Auction> auctions = adapter.read((ISqlReadStrategy) readfactory.createAllAuctionReadStrategy());
           //test add
           Auction auction = new Auction();
           auction.setDescription("Test");
           auction.setName("Auction" + auctions.size());
           auction.setScheduledDate(LocalDateTime.now());
           auction.setReleaseDate(LocalDateTime.now());
           adapter.insert(auction);
           //test update 
           auction.setId(auctions.size());
           auction.setName("Replacement");
           adapter.update(auction);
           //add Trenner
           auction.setDescription("Trenner");
           auction.setName("Trenner");
           auction.setId(auctions.size() + 1);
           adapter.insert(auction);
           //test delete
           adapter.delete(auction);
           //test read single
           auctions = adapter.read((ISqlReadStrategy) readfactory.createSingleAuctionReadStrategy(auctions.size()));
           auctions.forEach(i -> {
                System.out.println(i.getName());
           });
           return true;
        }
        catch(Exception e){
            return false;
        }
    }
    //ACHTUNG, WERTE MÜSSEN MOMENTAN HÄNDISCH EINGETRAGEN WERDEN
    public boolean testKoppel(){
        try{
           //sicherstellen, dass benötigte Elemente vorliegen
           //this.testAuction();
           //this.testAuction();
           //test add koppel
           Koppel koppel = new Koppel();
           Auction auction = new Auction();
           auction.setId(19);
           Product product = new Product();
           product.setId(9);
           koppel.setAuction(auction);
           koppel.setProduct(product);
           koppel.setMinimum_bid(0);
           koppel.setIs_sold(false);
           KoppelSqlAdapter adapter = (KoppelSqlAdapter) factory.createKoppelAdapter();
           adapter.insert(koppel);
           //test update
           koppel.setIs_sold(true);
           adapter.update(koppel);
           //test read by product
           List<Koppel> koppels = adapter.read((ISqlReadStrategy) readfactory.createKoppelReadStrategy_ByProduct(2));
           //test read by auction
           koppels = adapter.read((ISqlReadStrategy) readfactory.createKoppelReadStrategy_ByAuction(2));
           //test delete
           adapter.delete(koppel);
           return true;
        }
        catch(Exception e){
            return false;
        }
    }
}
