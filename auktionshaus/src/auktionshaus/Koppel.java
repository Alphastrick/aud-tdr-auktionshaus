package auktionshaus;

public class Koppel {
    
    private Auction auction;
    private Product product;
    private float minimum_bid;
    private boolean is_sold;

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public float getMinimum_bid() {
        return minimum_bid;
    }

    public void setMinimum_bid(float minimum_bid) {
        this.minimum_bid = minimum_bid;
    }

    public boolean isIs_sold() {
        return is_sold;
    }

    public void setIs_sold(boolean is_sold) {
        this.is_sold = is_sold;
    }
    
}
