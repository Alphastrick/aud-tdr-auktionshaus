package auktionshaus;

import java.time.LocalDateTime;
import java.util.*;

public class Auction implements IDataItem {
    
    private int id = -1;
    private String name;
    private String description;
    private LocalDateTime scheduledDate;
    private LocalDateTime releaseDate;
    private List<Product> products = new ArrayList<Product>();
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(LocalDateTime scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    
    @Override
    public void acceptVisitor(IDataVisitor visitor) {
        visitor.visitAuction(this);
    }
    
}
