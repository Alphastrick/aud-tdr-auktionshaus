package auktionshaus.employeeui;


public interface IModelObserver {

    void update(Model model);
    
}
