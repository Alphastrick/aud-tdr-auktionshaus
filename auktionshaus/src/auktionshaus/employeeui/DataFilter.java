package auktionshaus.employeeui;

import auktionshaus.Auction;
import auktionshaus.IDataItem;
import auktionshaus.IDataVisitor;
import auktionshaus.Product;
import java.util.ArrayList;
import java.util.List;

public class DataFilter implements IDataVisitor  {

    private String[] filter;
    private boolean doesMatch = false;
    private DataMask mask;
    
    public DataFilter(String filter, DataMask mask) {
        
        this.filter = filter.split(" ");
        this.mask = mask;
        
    }
    
    /**
     * Filter the given list of data items
     * @param items List of data items to filter
     * @return Filtert list of data items
     */
    public List<IDataItem> filter(List<IDataItem> items) {
        
        List<IDataItem> result = new ArrayList<>();
        
        items.forEach((item) -> {
            
            this.doesMatch = false;
            item.acceptVisitor(this);
            
            if (this.doesMatch) {
                result.add(item);
            }
            
        });
        
        return result;
    }
    
    /**
     * Test auction
     * @param auction Auction to test
     */
    @Override
    public void visitAuction(Auction auction) {
        
        if (this.mask.filterAuctions) {
            this.doesMatch = false;
        } else {
            for (String filterElement : this.filter) {
                this.doesMatch = auction.getName().contains(filterElement) || this.doesMatch;
                this.doesMatch = auction.getDescription().contains(filterElement) || this.doesMatch;
            }
        }
        
    }

    /**
     * Test product
     * @param product Product to test
     */
    @Override
    public void visitProduct(Product product) {
        
        if (this.mask.filterProducts) {
            this.doesMatch = false;
        } else {
            for (String filterElement : this.filter) {
                this.doesMatch = product.getName().contains(filterElement) || this.doesMatch;
                this.doesMatch = product.getDescription().contains(filterElement) || this.doesMatch;
            }
        }
        
    }
    
}
