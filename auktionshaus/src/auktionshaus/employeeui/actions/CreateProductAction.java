
package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;
import auktionshaus.Product;
import auktionshaus.employeeui.views.ProductEditor;
import statemachine.IAction;
import statemachine.TransitionContext;


public class CreateProductAction implements IAction {

    private final WindowContext windowContext;


    public CreateProductAction(WindowContext windowContext) {
        
        this.windowContext = windowContext;

    }

    @Override
    public String id() {

        return "CreateProductAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        Product p = new Product();
        p.setName("");
        p.setDescription("");
        p.setPriceEstimation(0.f);
        
        this.windowContext.setFrame(new ProductEditor(this.windowContext, p));

    }
    
    
}
