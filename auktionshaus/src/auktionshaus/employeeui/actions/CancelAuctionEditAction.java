package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.views.Overview;
import statemachine.IAction;
import statemachine.TransitionContext;


public class CancelAuctionEditAction implements IAction {

    private final WindowContext windowContext;


    public CancelAuctionEditAction(WindowContext windowContext) {
        
        this.windowContext = windowContext;

    }

    @Override
    public String id() {

        return "CancelAuctionEditAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        javax.swing.JFrame frame = new javax.swing.JFrame();
        int option = javax.swing.JOptionPane.showOptionDialog(frame,
            "Do you realy want to cancel?",
            "Confirm",
            javax.swing.JOptionPane.YES_NO_OPTION,
            javax.swing.JOptionPane.QUESTION_MESSAGE,
            null,
            null,
            null
        );

        if (option == javax.swing.JOptionPane.NO_OPTION) {
            context.blockTransition();
        } else {
            this.windowContext.setFrame(new Overview(this.windowContext));
        }

    }
    
    
}
