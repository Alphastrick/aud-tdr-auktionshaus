package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;
import auktionshaus.Product;
import auktionshaus.employeeui.views.ProductEditor;
import statemachine.IAction;
import statemachine.TransitionContext;


public class EditProductAction implements IAction {

    private final WindowContext windowContext;
    private final Product targetProduct;


    public EditProductAction(WindowContext windowContext, Product product) {
        
        this.windowContext = windowContext;
        this.targetProduct = product;

    }

    @Override
    public String id() {

        return "EditProductAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        this.windowContext.setFrame(new ProductEditor(this.windowContext, this.targetProduct));

    }
    
    
}
