package auktionshaus.employeeui.actions;

import auktionshaus.Auction;
import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.views.Overview;
import statemachine.IAction;
import statemachine.TransitionContext;


public class SaveAuctionAction implements IAction {

    private final WindowContext windowContext;
    private final Auction targetAuction;

    public SaveAuctionAction(WindowContext windowContext, Auction targetAuction) {
        
        this.windowContext = windowContext;
        this.targetAuction = targetAuction;

    }

    @Override
    public String id() {

        return "SaveAuctionAction";

    }

    @Override
    public void performIn(TransitionContext context) {
        
        if (this.targetAuction.getId() == -1) {
            this.windowContext.getController().getDataModel().insertAuction(this.targetAuction);
        } else {
            this.windowContext.getController().getDataModel().updateAuction(this.targetAuction);
        }

        this.windowContext.setFrame(new Overview(this.windowContext));
        
    }
    
    
}
