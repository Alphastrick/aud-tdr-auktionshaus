package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.views.Overview;
import statemachine.IAction;
import statemachine.TransitionContext;


public class StartupAction implements IAction {

    private final WindowContext windowContext;


    public StartupAction(WindowContext windowContext) {
        
        this.windowContext = windowContext;

    }

    @Override
    public String id() {

        return "StartupAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        this.windowContext.setFrame(new Overview(this.windowContext));

    }

}
