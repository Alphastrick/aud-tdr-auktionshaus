package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;
import auktionshaus.Auction;
import auktionshaus.employeeui.views.AuctionEditor;
import java.time.LocalDateTime;
import statemachine.IAction;
import statemachine.TransitionContext;


public class CreateAuctionAction implements IAction {

    private final WindowContext windowContext;


    public CreateAuctionAction(WindowContext windowContext) {
        
        this.windowContext = windowContext;

    }

    @Override
    public String id() {

        return "CreateAuctionAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        Auction a = new Auction();
        a.setName("");
        a.setDescription("");
        a.setReleaseDate(LocalDateTime.now());
        a.setScheduledDate(LocalDateTime.now());
        
        this.windowContext.setFrame(new AuctionEditor(this.windowContext, a));

    }
    
    
}
