package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;

import auktionshaus.Product;
import auktionshaus.employeeui.views.Overview;
import statemachine.IAction;
import statemachine.TransitionContext;


public class SaveProductAction implements IAction {

    private final WindowContext windowContext;
    private final Product targetProduct;


    public SaveProductAction(WindowContext windowContext, Product targetProduct) {
        
        this.windowContext = windowContext;
        this.targetProduct = targetProduct;

    }

    @Override
    public String id() {

        return "EditProductAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        if (this.targetProduct.getId() == -1) {
            this.windowContext.getController().getDataModel().insertProduct(this.targetProduct);
        } else {
            this.windowContext.getController().getDataModel().updateProduct(this.targetProduct);
        }

        this.windowContext.setFrame(new Overview(this.windowContext));
    }
    
    
}
