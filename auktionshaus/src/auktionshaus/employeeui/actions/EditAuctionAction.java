package auktionshaus.employeeui.actions;

import auktionshaus.employeeui.WindowContext;
import auktionshaus.Auction;
import auktionshaus.employeeui.views.AuctionEditor;
import statemachine.IAction;
import statemachine.TransitionContext;


public class EditAuctionAction implements IAction {

    private final WindowContext windowContext;
    private final Auction targetAuction;


    public EditAuctionAction(WindowContext windowContext, Auction auction) {
        
        this.windowContext = windowContext;
        this.targetAuction = auction;

    }

    @Override
    public String id() {

        return "EditAuctionAction";

    }

    @Override
    public void performIn(TransitionContext context) {

        this.windowContext.setFrame(new AuctionEditor(this.windowContext, this.targetAuction));

    }
    
    
}
