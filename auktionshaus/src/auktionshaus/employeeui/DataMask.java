package auktionshaus.employeeui;

public class DataMask {
 
    public final boolean filterProducts;
    public final boolean filterAuctions;
    
    public DataMask(boolean filterProducts, boolean filterAuctions) {
        this.filterProducts = filterProducts;
        this.filterAuctions = filterAuctions;
    }

}
