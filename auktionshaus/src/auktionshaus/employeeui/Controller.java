package auktionshaus.employeeui;

import auktionshaus.Persistence.SqlAdapterFactory;
import auktionshaus.Persistence.SqlReadStrategyFactory;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    
    private final List<IModelObserver> observers = new ArrayList<>();
    private final Model dataModel;
    
    public Controller() {
        
         this.dataModel = new Model(this, new SqlAdapterFactory("auktionshaus_user", "1234"), new SqlReadStrategyFactory());
        
    }
    
    /**
     * Acces the controllers data model
     * @return The associated data model
     */
    public Model getDataModel() {
        return this.dataModel;
    }
    
    /*
     * Updates all subscribers
     */
    public void update() {
        this.notifyAllObservers();
    }
    
    /**
     * Adds a observer to the controller
     * @param observer Oberver to add
     */
    public void addObserver(IModelObserver observer) {
        
        if (!this.observers.contains(observer)) {
            this.observers.add(observer);
            observer.update(this.dataModel);
        }
        
    }
    
    /**
     * Remove observer from the controller
     * @param observer Oberver to remove
     */
    public void removeObserver(IModelObserver observer) {
        
        if (this.observers.contains(observer)) {
            this.observers.remove(observer);
        }
        
    }
    
    /**
     * Notifing all obersvers
     */
    private void notifyAllObservers() {
        this.observers.forEach((observer) -> {
            observer.update(this.dataModel);
        });
    }
    
}
