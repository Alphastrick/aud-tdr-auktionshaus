package auktionshaus.employeeui;

import auktionshaus.*;
import auktionshaus.Persistence.Interfaces.*;
import java.util.List;

public class Model {
    
    private final Controller controller;
    private final IAdapterFactory adapterFactory;
    private final IReadStrategyFactory strategyFactory;
    
    
    public Model(Controller controller, IAdapterFactory adapterFactory, IReadStrategyFactory strategyFactory) {
        
        this.adapterFactory = adapterFactory;
        this.strategyFactory = strategyFactory;
        this.controller = controller;
        
    }
    
    /**
     * Inserts the given Product from the database and calls update
     * @param a Auction to insert
     */
    public void insertAuction(Auction a) {
        
        IAuctionAdapter adapter = this.adapterFactory.createAuctionAdapter();
        adapter.insert(a);
        
        IKoppelAdapter koppelAdapter = this.adapterFactory.createKoppelAdapter();
        a.getProducts().forEach((p) -> {
            Koppel k = new Koppel();
            k.setAuction(a);
            k.setProduct(p);
            k.setMinimum_bid(0);
            k.setIs_sold(false);
            koppelAdapter.insert(k);
        });
        
        this.controller.update();
        
    }
  
     /**
     * Updates the given Auction in the database and calls update
     * @param a Auction to update
     */
    public void updateAuction(Auction a) {
        
        IAuctionAdapter adapter = this.adapterFactory.createAuctionAdapter();
        adapter.update(a);
        
        IKoppelAdapter koppelAdapter = this.adapterFactory.createKoppelAdapter();
        
        // Delete all links
        IKoppelReadStrategy koppelStrat = this.strategyFactory.createKoppelReadStrategy_ByAuction(a.getId());
        List<Koppel> pairs = koppelAdapter.read(koppelStrat);
        pairs.forEach(pair -> koppelAdapter.delete(pair));

        // Insert all again
        a.getProducts().forEach((p) -> {
            Koppel k = new Koppel();
            k.setAuction(a);
            k.setProduct(p);
            k.setMinimum_bid(0);
            k.setIs_sold(false);
            koppelAdapter.insert(k);
        });
        
        this.controller.update();
        
    }
    
    
    /**
     * Deletes the given Auction from the database and calls update
     * @param a Auction to delete
     */
    public void deleteAuction(Auction a) {
        
        IAuctionAdapter adapter = this.adapterFactory.createAuctionAdapter();
        adapter.delete(a);
        this.controller.update();
        
    }
    
    /**
     * Fetches a list of auctions from the database
     * @return A list of all auctions
     */
    public List<Auction> getAuctions() {
        
        IAuctionAdapter adapter = this.adapterFactory.createAuctionAdapter();
        IAuctionReadStrategy strat = this.strategyFactory.createAllAuctionReadStrategy();
        
        List<Auction> auctions = adapter.read(strat);
        
        IKoppelAdapter koppelAdapter = this.adapterFactory.createKoppelAdapter();
        auctions.forEach((a) -> {
            IKoppelReadStrategy koppelStrat = this.strategyFactory.createKoppelReadStrategy_ByAuction(a.getId());
            List<Koppel> pairs = koppelAdapter.read(koppelStrat);
            
            pairs.forEach((k) -> {
                a.getProducts().add(k.getProduct());
            });
        });

        return auctions;
        
    }
    
    /**
     * Inserts the given Product from the database and calls update
     * @param p Product to insert
     */
    public void insertProduct(Product p) {
        
        IProductAdapter adapter = this.adapterFactory.createProductAdapter();
        adapter.insert(p);
        
        this.controller.update();
        
    }
    
    /**
     * Updates the given Product in the database and calls update
     * @param p Product to update
     */
    public void updateProduct(Product p) {
        
        IProductAdapter adapter = this.adapterFactory.createProductAdapter();
        adapter.update(p);
        
        this.controller.update();
        
    }
    
    /**
     * Deletes the given Product from the database and calls update
     * @param p Product to delete
     */
    public void deleteProduct(Product p) {
        
        IProductAdapter adapter = this.adapterFactory.createProductAdapter();
        adapter.delete(p);
        
        this.controller.update();
        
    }
    
    /**
     * Fetches a list of products from the database
     * @return A list of all produtcs
     */
    public List<Product> getProducts() {
        
        IProductAdapter adapter = this.adapterFactory.createProductAdapter();
        IProductReadStrategy strat = this.strategyFactory.createAllProductReadStrategy();
        return adapter.read(strat);
        
    }
    
}
