package auktionshaus.employeeui.views;

import auktionshaus.employeeui.DataFilter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import auktionshaus.Auction;
import auktionshaus.IDataItem;
import auktionshaus.Persistence.Interfaces.*;
import auktionshaus.Persistence.*;
import auktionshaus.Product;

public class OverviewListModel extends javax.swing.AbstractListModel<String> {
    
    private List<IDataItem> items = new ArrayList<IDataItem>();
    private List<IDataItem> filteredItems = null; 
    private OverviewListItemNameExtractor nameExtractor = new OverviewListItemNameExtractor();
    
    public OverviewListModel() {

        super();

    }

    public void setItems(List<IDataItem> items) {
        this.items = items;
    }
    
    public void applyFilter(DataFilter filter) {
    
        this.filteredItems = filter.filter(this.items);
        
    }
    
    public void resetFilter() {
        this.filteredItems = null;
    }
    
    public int getSize() {
        
        if (filteredItems != null) {
            return this.filteredItems.size();
        } else {
            return this.items.size();
        }
    }
    
    public IDataItem getItemAt(int i) {
        
        if (filteredItems != null) {
            return this.filteredItems.get(i);
        } else {
            return this.items.get(i);
        }
        
    }
    
    public String getElementAt(int i) {
        
        if (filteredItems != null) {
            this.filteredItems.get(i).acceptVisitor(this.nameExtractor);
        } else {
            this.items.get(i).acceptVisitor(this.nameExtractor);
        }
        
        return this.nameExtractor.getNameOfLastVisitedItem();
        
    }

}
