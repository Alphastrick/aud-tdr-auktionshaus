package auktionshaus.employeeui.views;

import auktionshaus.Product;
import auktionshaus.employeeui.IFrame;
import auktionshaus.employeeui.IModelObserver;
import auktionshaus.employeeui.Model;
import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.actions.CancelProductEditAction;
import auktionshaus.employeeui.actions.SaveProductAction;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

public class ProductEditor extends javax.swing.JPanel implements IFrame, IModelObserver {

    private WindowContext windowContext;
    private Product target;
    private BufferedImage selectedImage = null;
    
    public ProductEditor(WindowContext windowContext, Product product) {
        
        initComponents();
        
        this.windowContext = windowContext;
        this.windowContext.getController().addObserver(this);
        this.target = product;
        this.setFormValues(product);
        
    }

    private void setFormValues(Product product) {
        
        byte[] img = this.target.getImage();
        if (img != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(img);
            try {
                this.selectedImage = ImageIO.read(bais);
                this.viewSelectedImageButton.setEnabled(true);
            } catch (Exception e) {
                
                System.err.println(e);
            
                javax.swing.JFrame frame = new javax.swing.JFrame();
                int option = javax.swing.JOptionPane.showOptionDialog(frame,
                    "Failed to load image!",
                    "Error",
                    javax.swing.JOptionPane.OK_OPTION,
                    javax.swing.JOptionPane.ERROR_MESSAGE,
                    null,
                    null,
                    null
                );
                
            }
        } else {
            this.viewSelectedImageButton.setEnabled(false);
        }
        
        this.nameInput.setText(product.getName());
        this.descriptionInput.setText(product.getDescription());
        this.priceInput.setText(String.format("%.2f", product.getPriceEstimation()));
        
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new java.awt.Panel();
        nameLabel = new javax.swing.JLabel();
        nameInput = new javax.swing.JTextField();
        descriptionLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        descriptionInput = new javax.swing.JTextArea();
        priceLabel = new javax.swing.JLabel();
        priceInput = new javax.swing.JTextField();
        imagePanel = new javax.swing.JPanel();
        imageLabel = new javax.swing.JLabel();
        imageButton = new javax.swing.JToggleButton();
        viewSelectedImageButton = new javax.swing.JButton();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        cancelButton = new javax.swing.JToggleButton();
        saveButton = new javax.swing.JToggleButton();
        jSeparator1 = new javax.swing.JSeparator();

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        nameLabel.setText("Name");

        descriptionLabel.setText("Description");

        descriptionInput.setColumns(20);
        descriptionInput.setRows(5);
        jScrollPane1.setViewportView(descriptionInput);

        priceLabel.setText("Price Estimation (€)");

        imageLabel.setText("Image");

        imageButton.setText("Select Image");
        imageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSelectImage(evt);
            }
        });

        viewSelectedImageButton.setText("View Image");
        viewSelectedImageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onViewImage(evt);
            }
        });

        javax.swing.GroupLayout imagePanelLayout = new javax.swing.GroupLayout(imagePanel);
        imagePanel.setLayout(imagePanelLayout);
        imagePanelLayout.setHorizontalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanelLayout.createSequentialGroup()
                .addComponent(imageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 167, Short.MAX_VALUE)
                .addComponent(viewSelectedImageButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(imageButton))
        );
        imagePanelLayout.setVerticalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanelLayout.createSequentialGroup()
                .addGroup(imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(imageLabel)
                    .addGroup(imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(imageButton)
                        .addComponent(viewSelectedImageButton)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCancel(evt);
            }
        });

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSave(evt);
            }
        });

        jLayeredPane2.setLayer(cancelButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(saveButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jSeparator1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSeparator1)
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(saveButton)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(nameInput)
                    .addComponent(nameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLayeredPane2)
                    .addComponent(imagePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(priceInput)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descriptionLabel)
                            .addComponent(priceLabel))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(descriptionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(priceLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(priceInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imagePanel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void onSelectImage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSelectImage
        
        JFrame frame = new JFrame("");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
        
            public String getDescription() {
                return "Images (.png)";
            }

            public boolean accept(File file) {
                return file.getName().toLowerCase().endsWith(".png") || file.isDirectory();
            }
            
        });
        
        int option = fileChooser.showOpenDialog(frame);
        
        if(option == JFileChooser.APPROVE_OPTION) {
            
           File file = fileChooser.getSelectedFile();
           System.out.println("File Selected: " + file.getName());
           
           try {
               
               this.selectedImage = ImageIO.read(file);
               this.viewSelectedImageButton.setEnabled(true);
               this.updateUI();
               
           } catch (Exception e) {
               
           }
           
        } else {
            
            System.out.println("Open command canceled");
            
        }
        
    }//GEN-LAST:event_onSelectImage

    private void onCancel(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onCancel
    
        try {
            this.windowContext.getStateMachine().dispatch(new CancelProductEditAction(this.windowContext));
            this.windowContext.getController().removeObserver(this);
        } catch (Exception e) {
            System.err.println(e);
        }

    }//GEN-LAST:event_onCancel

    private void onSave(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSave
       
        try {
            
            this.target.setName(this.nameInput.getText());
            this.target.setDescription(this.descriptionInput.getText());
            this.target.setPriceEstimation(Float.parseFloat(this.priceInput.getText().replace(",", ".")));

            if (this.selectedImage != null) {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                ImageIO.write(this.selectedImage, "png", out);
                this.target.setImage(out.toByteArray());
            }
            
            this.windowContext.getStateMachine().dispatch(new SaveProductAction(this.windowContext, this.target));
            this.windowContext.getController().removeObserver(this);
            
        } catch (Exception e) {
            
            javax.swing.JFrame frame = new javax.swing.JFrame();
            int option = javax.swing.JOptionPane.showOptionDialog(frame,
                String.format("Failed to save Product! (%s)", e.getMessage()),
                "Error",
                javax.swing.JOptionPane.OK_OPTION,
                javax.swing.JOptionPane.ERROR_MESSAGE,
                null,
                null,
                null
            );
            
        }
        
    }//GEN-LAST:event_onSave

    private void onViewImage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onViewImage
        
        final int MAX_SIZE = 512;
        
        float width = this.selectedImage.getWidth();
        float height = this.selectedImage.getHeight();
        
        float ratio = width / height;
        
        if (ratio > 1) {
            width = MAX_SIZE;
            height = MAX_SIZE / ratio;
        } else {
            width = MAX_SIZE * ratio;
            height = MAX_SIZE;
        }
        
        ImageIcon icon = new ImageIcon(this.selectedImage.getScaledInstance((int)width, (int)height, (int)width));
        JLabel label = new JLabel(icon);
        JOptionPane.showMessageDialog(null, label, "Product Image", JOptionPane.PLAIN_MESSAGE);
        
    }//GEN-LAST:event_onViewImage


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton cancelButton;
    private javax.swing.JTextArea descriptionInput;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JToggleButton imageButton;
    private javax.swing.JLabel imageLabel;
    private javax.swing.JPanel imagePanel;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField nameInput;
    private javax.swing.JLabel nameLabel;
    private java.awt.Panel panel1;
    private javax.swing.JTextField priceInput;
    private javax.swing.JLabel priceLabel;
    private javax.swing.JToggleButton saveButton;
    private javax.swing.JButton viewSelectedImageButton;
    // End of variables declaration//GEN-END:variables

    @Override
    public String getTitle() {

        return "Edit Product";

    }

    @Override
    public JComponent getComponent() {

        return this;

    }

    public void update(Model model) {
        
    }

}
