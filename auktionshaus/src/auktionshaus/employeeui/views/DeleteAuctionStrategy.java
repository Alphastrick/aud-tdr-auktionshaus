package auktionshaus.employeeui.views;

import auktionshaus.Auction;
import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.actions.EditProductAction;
import auktionshaus.employeeui.IDataStrategy;

public class DeleteAuctionStrategy implements IDataStrategy {

    private WindowContext windowContext;
    private Auction targetAuction;


    public DeleteAuctionStrategy(WindowContext context, Auction auction) {

        this.windowContext = context;
        this.targetAuction = auction;

    }

    @Override
    public String getStrategyName() {
        return "Delete Auction";
    }

    @Override
    public void execute() throws Exception {
        
        this.windowContext.getController().getDataModel().deleteAuction(targetAuction);
        
    }
    
}
