package auktionshaus.employeeui.views;

import auktionshaus.Auction;
import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.actions.EditAuctionAction;
import auktionshaus.employeeui.IDataStrategy;

public class EditAuctionStrategy implements IDataStrategy {

    private WindowContext windowContext;
    private Auction targetAuction;


    public EditAuctionStrategy(WindowContext context, Auction auction) {

        this.windowContext = context;
        this.targetAuction = auction;

    }

    @Override
    public String getStrategyName() {
        return "Edit Auction";
    }

    @Override
    public void execute() throws Exception {
        
        this.windowContext.getStateMachine().dispatch(new EditAuctionAction(this.windowContext, this.targetAuction));
        
    }

}
