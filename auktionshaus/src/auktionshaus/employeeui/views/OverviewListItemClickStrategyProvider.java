package auktionshaus.employeeui.views;

import auktionshaus.employeeui.IDataStrategy;
import java.util.ArrayList;
import java.util.List;

import auktionshaus.Auction;
import auktionshaus.IDataVisitor;
import auktionshaus.Product;
import auktionshaus.employeeui.WindowContext;


public class OverviewListItemClickStrategyProvider implements IDataVisitor {

    private WindowContext windowContext;
    private List<IDataStrategy> strategies = new ArrayList<IDataStrategy>();

    public OverviewListItemClickStrategyProvider(WindowContext context) {
        
        this.windowContext = context;
    
    }
    
    @Override
    public void visitAuction(Auction auction) {
        
        strategies.clear();
        strategies.add(new EditAuctionStrategy(this.windowContext, auction));
        strategies.add(new DeleteAuctionStrategy(this.windowContext, auction));
        
    }

    @Override
    public void visitProduct(Product product) {
        
        strategies.clear();
        strategies.add(new EditProductStrategy(this.windowContext, product));
        strategies.add(new DeleteProductStrategy(this.windowContext, product));
        
    }
    
    public List<IDataStrategy> getAvailableStrategiesForLastVisitedItem() {
        
        return this.strategies;
        
    }
    
}
