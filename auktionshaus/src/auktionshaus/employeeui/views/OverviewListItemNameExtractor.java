package auktionshaus.employeeui.views;

import auktionshaus.Auction;
import auktionshaus.IDataVisitor;
import auktionshaus.Product;


public class OverviewListItemNameExtractor implements IDataVisitor {

    private String name = "";
    
    
    @Override
    public void visitAuction(Auction auction) {
        
        this.name = auction.getName();
        
    }

    @Override
    public void visitProduct(Product product) {
        
        this.name = product.getName();
        
    }
    
    public String getNameOfLastVisitedItem() {
        
        return this.name;
        
    }
    
}
