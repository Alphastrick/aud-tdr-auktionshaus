package auktionshaus.employeeui.views;

import auktionshaus.Product;
import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.actions.EditProductAction;
import auktionshaus.employeeui.IDataStrategy;

public class EditProductStrategy implements IDataStrategy {

    private WindowContext windowContext;
    private Product targetProduct;


    public EditProductStrategy(WindowContext context, Product product) {

        this.windowContext = context;
        this.targetProduct = product;

    }

    @Override
    public String getStrategyName() {
        return "Edit Product";
    }

    @Override
    public void execute() throws Exception {

        this.windowContext.getStateMachine().dispatch(new EditProductAction(this.windowContext, this.targetProduct));
        
    }

}
