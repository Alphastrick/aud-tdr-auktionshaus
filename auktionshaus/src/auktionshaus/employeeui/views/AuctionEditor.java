package auktionshaus.employeeui.views;

import auktionshaus.Auction;
import auktionshaus.IDataItem;
import auktionshaus.Product;
import auktionshaus.employeeui.IFrame;
import auktionshaus.employeeui.IModelObserver;
import auktionshaus.employeeui.Model;
import auktionshaus.employeeui.WindowContext;
import auktionshaus.employeeui.actions.CancelAuctionEditAction;
import auktionshaus.employeeui.actions.SaveAuctionAction;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import statemachine.StateMachine;

public class AuctionEditor extends javax.swing.JPanel implements IFrame, IModelObserver {

    private final WindowContext windowContext;
    private final static DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");;
    private final Auction target;
    private final OverviewListModel productListModel;
    
    public AuctionEditor(WindowContext windowContext, Auction auction) {
        
        initComponents();
        this.windowContext = windowContext;
        this.windowContext.getController().addObserver(this);
        this.target = auction;
        this.productListModel = new OverviewListModel();
        
        this.setFormValues(auction);
    }

    /*
     * Fill the form with the values of an auction
     * @param auction Auction to set
     */
    private void setFormValues(Auction auction) {

         this.nameInput.setText(auction.getName());
         this.descriptionInput.setText(auction.getDescription());

        this.scheduledDateInput.setText(AuctionEditor.DATE_FORMATTER.format(auction.getScheduledDate()));
        this.releaseDateInput.setText(AuctionEditor.DATE_FORMATTER.format(auction.getReleaseDate()));
        
        List<IDataItem> products = new ArrayList<>();
        this.target.getProducts().forEach((item) -> products.add(item));
        this.productListModel.setItems(products);
        this.productsList.setModel(this.productListModel);
        this.productsList.updateUI();
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        nameLabel = new javax.swing.JLabel();
        nameInput = new javax.swing.JTextField();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        descriptionLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        descriptionInput = new javax.swing.JTextArea();
        jLayeredPane3 = new javax.swing.JLayeredPane();
        scheduleLabel = new javax.swing.JLabel();
        scheduledDateInput = new javax.swing.JTextField();
        jLayeredPane4 = new javax.swing.JLayeredPane();
        releaseLabel = new javax.swing.JLabel();
        releaseDateInput = new javax.swing.JTextField();
        jLayeredPane5 = new javax.swing.JLayeredPane();
        productsLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        productsList = new javax.swing.JList<>();
        removeProductButton = new javax.swing.JButton();
        addProductButton = new javax.swing.JButton();
        panel1 = new java.awt.Panel();
        cancelButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        nameLabel.setText("Name");

        jLayeredPane1.setLayer(nameLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(nameInput, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addComponent(nameLabel)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(nameInput)
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addComponent(nameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        descriptionLabel.setText("Description");

        descriptionInput.setColumns(20);
        descriptionInput.setRows(5);
        jScrollPane1.setViewportView(descriptionInput);

        jLayeredPane2.setLayer(descriptionLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addComponent(descriptionLabel)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
        );
        jLayeredPane2Layout.setVerticalGroup(
            jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane2Layout.createSequentialGroup()
                .addComponent(descriptionLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        scheduleLabel.setText("Scheduled Date / Time");

        jLayeredPane3.setLayer(scheduleLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane3.setLayer(scheduledDateInput, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane3Layout = new javax.swing.GroupLayout(jLayeredPane3);
        jLayeredPane3.setLayout(jLayeredPane3Layout);
        jLayeredPane3Layout.setHorizontalGroup(
            jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane3Layout.createSequentialGroup()
                .addComponent(scheduleLabel)
                .addContainerGap(394, Short.MAX_VALUE))
            .addComponent(scheduledDateInput)
        );
        jLayeredPane3Layout.setVerticalGroup(
            jLayeredPane3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane3Layout.createSequentialGroup()
                .addComponent(scheduleLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scheduledDateInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        releaseLabel.setText("Release Date / Time");

        jLayeredPane4.setLayer(releaseLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane4.setLayer(releaseDateInput, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane4Layout = new javax.swing.GroupLayout(jLayeredPane4);
        jLayeredPane4.setLayout(jLayeredPane4Layout);
        jLayeredPane4Layout.setHorizontalGroup(
            jLayeredPane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane4Layout.createSequentialGroup()
                .addComponent(releaseLabel)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(releaseDateInput)
        );
        jLayeredPane4Layout.setVerticalGroup(
            jLayeredPane4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane4Layout.createSequentialGroup()
                .addComponent(releaseLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(releaseDateInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        productsLabel.setText("Products");

        productsList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(productsList);

        removeProductButton.setText("Remove");
        removeProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onRemoveProduct(evt);
            }
        });

        addProductButton.setText("Add");
        addProductButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onAddProduct(evt);
            }
        });

        jLayeredPane5.setLayer(productsLabel, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane5.setLayer(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane5.setLayer(removeProductButton, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane5.setLayer(addProductButton, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane5Layout = new javax.swing.GroupLayout(jLayeredPane5);
        jLayeredPane5.setLayout(jLayeredPane5Layout);
        jLayeredPane5Layout.setHorizontalGroup(
            jLayeredPane5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
            .addGroup(jLayeredPane5Layout.createSequentialGroup()
                .addComponent(productsLabel)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jLayeredPane5Layout.createSequentialGroup()
                .addComponent(removeProductButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(addProductButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jLayeredPane5Layout.setVerticalGroup(
            jLayeredPane5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane5Layout.createSequentialGroup()
                .addComponent(productsLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jLayeredPane5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(removeProductButton)
                    .addComponent(addProductButton)))
        );

        panel1.setBackground(java.awt.SystemColor.menu);

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onCancel(evt);
            }
        });

        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSave(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 301, Short.MAX_VALUE)
                .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSeparator1)
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cancelButton)
                    .addComponent(saveButton)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLayeredPane1)
                    .addComponent(jLayeredPane2)
                    .addComponent(jLayeredPane3)
                    .addComponent(jLayeredPane5)
                    .addComponent(jLayeredPane4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLayeredPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLayeredPane5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void onSave(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSave
      
        try {
            
            this.target.setName(this.nameInput.getText());
            this.target.setDescription(this.descriptionInput.getText());
            this.target.setReleaseDate(LocalDateTime.parse(this.releaseDateInput.getText(), AuctionEditor.DATE_FORMATTER));
            this.target.setScheduledDate(LocalDateTime.parse(this.scheduledDateInput.getText(), AuctionEditor.DATE_FORMATTER));
            
            this.windowContext.getStateMachine().dispatch(new SaveAuctionAction(this.windowContext, this.target));
            this.windowContext.getController().removeObserver(this);
            
        } catch (Exception e) {

            javax.swing.JFrame frame = new javax.swing.JFrame();
            int option = javax.swing.JOptionPane.showOptionDialog(frame,
                String.format("Failed to save Auction! (%s)", e.getMessage()),
                "Error",
                javax.swing.JOptionPane.OK_OPTION,
                javax.swing.JOptionPane.ERROR_MESSAGE,
                null,
                null,
                null
            );
            
        }
        
    }//GEN-LAST:event_onSave

    private void onCancel(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onCancel
        
        try {
            this.windowContext.getStateMachine().dispatch(new CancelAuctionEditAction(this.windowContext));
            this.windowContext.getController().removeObserver(this);
        } catch (Exception e) {
            
            System.err.println(e);
            
        }
        
    }//GEN-LAST:event_onCancel

    private void onAddProduct(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onAddProduct
        
        List<IDataItem> products = new ArrayList<>();
        this.windowContext.getController().getDataModel().getProducts().forEach((item) -> {
            if (!this.target.getProducts().contains(item))
                products.add(item);
        });
        OverviewListModel model = new OverviewListModel();
        model.setItems(products);
        
        ListSelectDialogPanel panel = new ListSelectDialogPanel("Select a Product to add", model);
        
        int result = javax.swing.JOptionPane.showConfirmDialog(
            null,
            panel,
            "Add Product",
            javax.swing.JOptionPane.OK_CANCEL_OPTION,
            javax.swing.JOptionPane.PLAIN_MESSAGE
        );
       
        if (result == javax.swing.JOptionPane.OK_OPTION) {
            
            int index = panel.getSelection();
            this.target.getProducts().add((Product)products.get(index));
            this.setFormValues(this.target);
            
        }
        
    }//GEN-LAST:event_onAddProduct

    private void onRemoveProduct(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onRemoveProduct
        
        int selectedIndex = this.productsList.getSelectedIndex();
        if (selectedIndex != -1) {
            this.target.getProducts().remove(selectedIndex);
            this.setFormValues(this.target);
        }
        
    }//GEN-LAST:event_onRemoveProduct


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addProductButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextArea descriptionInput;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JLayeredPane jLayeredPane3;
    private javax.swing.JLayeredPane jLayeredPane4;
    private javax.swing.JLayeredPane jLayeredPane5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField nameInput;
    private javax.swing.JLabel nameLabel;
    private java.awt.Panel panel1;
    private javax.swing.JLabel productsLabel;
    private javax.swing.JList<String> productsList;
    private javax.swing.JTextField releaseDateInput;
    private javax.swing.JLabel releaseLabel;
    private javax.swing.JButton removeProductButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel scheduleLabel;
    private javax.swing.JTextField scheduledDateInput;
    // End of variables declaration//GEN-END:variables

    @Override
    public String getTitle() {

        return "Edit Auction";

    }

    @Override
    public JComponent getComponent() {

        return this;

    }

    public void update(Model model) { }

}
