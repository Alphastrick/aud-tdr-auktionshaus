package auktionshaus.employeeui.views;

import auktionshaus.employeeui.IDataStrategy;
import auktionshaus.Product;
import auktionshaus.employeeui.WindowContext;

public class DeleteProductStrategy implements IDataStrategy {

    private WindowContext windowContext;
    private Product targetProduct;


    public DeleteProductStrategy(WindowContext context, Product product) {

        this.windowContext = context;
        this.targetProduct = product;

    }

    @Override
    public String getStrategyName() {
        return "Delete Product";
    }

    @Override
    public void execute() throws Exception {
        
        this.windowContext.getController().getDataModel().deleteProduct(targetProduct);
        
    }
    
}
