package auktionshaus.employeeui;

public interface IFrame {
    
    String getTitle();
    javax.swing.JComponent getComponent();
    
}
