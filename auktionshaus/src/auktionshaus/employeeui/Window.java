package auktionshaus.employeeui;

import auktionshaus.employeeui.actions.StartupAction;
import statemachine.StateMachine;
import statemachine.Transition;
import statemachine.State;
import java.util.HashMap;


public class Window extends javax.swing.JFrame {

    public Window() {
        initComponents();  
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 592, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 538, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Set the window component (body)
     * @param component Component to display
     */
    public void set(javax.swing.JComponent component) {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(component, javax.swing.GroupLayout.DEFAULT_SIZE, 592, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(component, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
        );
        
    }
    
    /**
     * Clears the body of the window
     */
    public void clear() {
        
        getContentPane().removeAll();
        
    }
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            Window window;
            Controller controller;
            StateMachine stateMachine;
            WindowContext context;
            
            try {
                
                window = new Window();
                controller = new Controller();
                
                HashMap<String, Transition> transitions = new HashMap<>();
                transitions.put("StartupAction", new Transition(new State("InitState"), new State("OverviewState")));
                transitions.put("CreateAuctionAction", new Transition(new State("OverviewState"), new State("AuctionEditState")));
                transitions.put("EditAuctionAction", new Transition(new State("OverviewState"), new State("AuctionEditState")));
                transitions.put("CancelAuctionEditAction", new Transition(new State("AuctionEditState"), new State("OverviewState")));
                transitions.put("SaveAuctionAction", new Transition(new State("AuctionEditState"), new State("OverviewState")));
                transitions.put("CreateProductAction", new Transition(new State("OverviewState"), new State("ProductEditState")));
                transitions.put("EditProductAction", new Transition(new State("OverviewState"), new State("ProductEditState")));
                transitions.put("CancelProductEditAction", new Transition(new State("ProductEditState"), new State("OverviewState")));
                transitions.put("SaveProductEditAction", new Transition(new State("ProductEditState"), new State("OverviewState")));
                
                stateMachine = new StateMachine(new State("InitState"), transitions);
                context = new WindowContext(window, stateMachine, controller);
                
                stateMachine.dispatch(new StartupAction(context));
                
            } catch (Exception e) {
                
                System.out.println("Failed to start application: " + e.getMessage());
                
                javax.swing.JOptionPane.showOptionDialog(null,
                    String.format("Failed to start application.\nCheck if the database is up an running."),
                    "Error",
                    javax.swing.JOptionPane.OK_OPTION,
                    javax.swing.JOptionPane.ERROR_MESSAGE,
                    null,
                    null,
                    null
                );
                
                System.exit(0);
                return;
                
            }
            
            window.setVisible(true);
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
