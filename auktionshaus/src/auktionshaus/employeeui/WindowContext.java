package auktionshaus.employeeui;

import statemachine.*;

public class WindowContext {
 
    private final Window window;
    private final StateMachine stateMachine;
    private final Controller controller;

    
    public WindowContext(Window window, StateMachine stateMachine, Controller controller) {
        
        this.window = window;
        this.stateMachine = stateMachine;
        this.controller = controller;
        
    }
    
    /**
     * Changes the current frame to the given frame
     * @param frame Frame to set
     */
    public void setFrame(IFrame frame) {
        
        this.window.clear();
        this.window.setTitle(frame.getTitle());
        this.window.set(frame.getComponent());
        
    }

    /**
     * Access the applications state machine
     * @return Applications state machine
     */
    public StateMachine getStateMachine() {

        return this.stateMachine;

    }
    
    /**
     * Access the applications controller
     * @return Applications controller
     */
    public Controller getController() {

        return this.controller;

    }
    
}
