package auktionshaus.employeeui;

public interface IDataStrategy {

    String getStrategyName();
    void execute() throws Exception;

}
