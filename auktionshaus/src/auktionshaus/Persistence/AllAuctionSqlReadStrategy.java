package auktionshaus.Persistence;

import auktionshaus.Auction;
import auktionshaus.Persistence.Interfaces.IAuctionReadStrategy;
import java.util.List;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import java.sql.*;
import javax.sql.rowset.CachedRowSet;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AllAuctionSqlReadStrategy implements IAuctionReadStrategy, ISqlReadStrategy{

    private SqlDatabaseWrapper connection;
    
    @Override
    public List<Auction> execute() {
        String query = "SELECT scheduled_date, scheduled_release, id, name, description FROM Auctions ORDER BY id";
        String[] types = {};
        String[] content = {};
        CachedRowSet result = null;
        try {
            result = connection.read(types, content, query);
        } catch (SQLException ex) {
            Logger.getLogger(AllAuctionSqlReadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        List <Auction> auctions = new LinkedList();
        try {
            while(result.next()){
                Auction a = new Auction();
                a.setScheduledDate(result.getTimestamp(1).toLocalDateTime());
                a.setReleaseDate(result.getTimestamp(2).toLocalDateTime());
                a.setId(result.getInt(3));
                a.setName(result.getString(4));
                a.setDescription(result.getString(5));
                auctions.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AllAuctionSqlReadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return auctions;
    }

    @Override
    public void prepare(SqlDatabaseWrapper connection) {
        this.connection = connection;
    }
    
}
