package auktionshaus.Persistence;

import auktionshaus.Koppel;
import auktionshaus.Persistence.Interfaces.IProductAdapter;
import auktionshaus.Persistence.Interfaces.IProductReadStrategy;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import auktionshaus.Product;
import java.util.List;
import java.util.Base64;


public class ProductSqlAdapter <T extends ISqlReadStrategy & IProductReadStrategy> implements IProductAdapter<T>{

    private final SqlDatabaseWrapper connection;
    public ProductSqlAdapter(SqlDatabaseWrapper connection) {
        
        this.connection = connection;
        
    }
    
    @Override
    public void insert(Product product) {
        
        String query = "INSERT INTO Products (image, price_estimation, name, description) VALUES (?,?,?,?)";
        String[] types = {"Image","float","String","String"};
        String[] content = new String[4];
        content[0] = Base64.getEncoder().encodeToString(product.getImage());
        content[1] = Float.toString(product.getPriceEstimation());
        content[2] = product.getName();
        content[3] = product.getDescription();
        connection.update(types, content, query);
        
    }

    @Override
    public void delete(Product product) {
        
        //using koppel adapter to delete any entries with this product
        SqlAdapterFactory factory = new SqlAdapterFactory("auktionshaus_user","1234");
        KoppelSqlAdapter adapter =  (KoppelSqlAdapter) factory.createKoppelAdapter();
        SqlReadStrategyFactory readfactory = new SqlReadStrategyFactory();
        List<Koppel> koppel = adapter.read((ISqlReadStrategy) readfactory.createKoppelReadStrategy_ByProduct(product.getId()));
        koppel.forEach(k ->{
            adapter.delete(k);
        });
        
        String query = "DELETE FROM Products WHERE id = ?";
        String[] types = {"int"};
        String[] content = new String[1];
        content[0] = String.valueOf(product.getId());
        connection.update(types, content, query);
        
    }

    @Override
    public void update(Product product) {
        
        String query = "UPDATE Products SET image = ?, price_estimation = ?, name = ?, description = ? WHERE id = ?";
        String[] types = {"Image","float","String","String","int"};
        String[] content = new String[5];
        content[0] = Base64.getEncoder().encodeToString(product.getImage());
        content[1] = Float.toString(product.getPriceEstimation());
        content[2] = product.getName();
        content[3] = product.getDescription();
        content[4] = String.valueOf(product.getId());
        connection.update(types, content, query);
        
    }

    @Override
    public List<Product> read(T readstrategy) {
        
        readstrategy.prepare(connection);
        return readstrategy.execute();
        
    }
    
}
