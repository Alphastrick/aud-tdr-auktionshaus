package auktionshaus.Persistence;

import auktionshaus.Auction;
import auktionshaus.Koppel;
import auktionshaus.Persistence.Interfaces.IAuctionAdapter;
import auktionshaus.Persistence.Interfaces.IAuctionReadStrategy;
import java.util.List;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;

public class AuctionSqlAdapter <T extends ISqlReadStrategy & IAuctionReadStrategy> implements IAuctionAdapter<T>{
    
    private final SqlDatabaseWrapper connection;
    
    public AuctionSqlAdapter(SqlDatabaseWrapper connection) {
        this.connection = connection;
    }

    @Override
    public void insert(Auction auction) {
        
        String query = "INSERT INTO Auctions (scheduled_date, scheduled_release, name, description) VALUES (?,?,?,?)";
        String[] types = {"Date","Date","String","String"};
        String[] content = new String[4];
        content[0] = auction.getScheduledDate().toString();
        content[1] = auction.getReleaseDate().toString();
        content[2] = auction.getName();
        content[3] = auction.getDescription();
        connection.update(types, content, query);
        
    }

    @Override
    public void delete(Auction auction) {
        
        //using koppel adapter to delete any entries with this auction
        SqlAdapterFactory factory = new SqlAdapterFactory("auktionshaus_user","1234");
        KoppelSqlAdapter adapter =  (KoppelSqlAdapter) factory.createKoppelAdapter();
        SqlReadStrategyFactory readfactory = new SqlReadStrategyFactory();
        List<Koppel> koppel = adapter.read((ISqlReadStrategy) readfactory.createKoppelReadStrategy_ByAuction(auction.getId()));
        koppel.forEach(k ->{
            adapter.delete(k);
        });
        
        String query = "DELETE FROM Auctions WHERE id = ?";
        String[] types = {"int"};
        String[] content = new String[1];
        content[0] = String.valueOf(auction.getId());
        connection.update(types, content, query);

    }

    @Override
    public void update(Auction auction) {
        
        String query = "UPDATE Auctions SET scheduled_date = ?, scheduled_release = ?, name = ?, description = ? WHERE id = ?";
        String[] types = {"Date","Date","String","String","int"};
        String[] content = new String[5];
        content[0] = auction.getScheduledDate().toString();
        content[1] = auction.getReleaseDate().toString();
        content[2] = auction.getName();
        content[3] = auction.getDescription();
        content[4] = String.valueOf(auction.getId());
        connection.update(types, content, query);
        
    }

    @Override
    public List read(T readstrategy) {
       
        readstrategy.prepare(connection);
        return readstrategy.execute();
       
    }
  
}
