package auktionshaus.Persistence;

import auktionshaus.Koppel;
import auktionshaus.Persistence.Interfaces.IKoppelAdapter;
import auktionshaus.Persistence.Interfaces.IKoppelReadStrategy;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import java.util.List;

public class KoppelSqlAdapter <T extends ISqlReadStrategy & IKoppelReadStrategy> implements IKoppelAdapter<T>{

    private final SqlDatabaseWrapper connection;

    public KoppelSqlAdapter(SqlDatabaseWrapper connection) {
        this.connection = connection;
    }
    
    @Override
    public void insert(Koppel koppel) {
        String query = "INSERT INTO Listings (auction_id, product_id, minimum_bid, is_sold) VALUES (?,?,?,?)";
        String[] types = {"int","int","float","boolean"};
        String[] content = new String[4];
        content[0] = Integer.toString(koppel.getAuction().getId());
        content[1] = Integer.toString(koppel.getProduct().getId());
        content[2] = Float.toString(koppel.getMinimum_bid());
        content[3] = Boolean.toString(koppel.isIs_sold());
        connection.update(types, content, query);
    }

    @Override
    public void delete(Koppel koppel) {
        String query = "DELETE FROM Listings WHERE auction_id = ? AND product_id = ?";
        String[] types = {"int", "int"};
        String[] content = new String[2];
        content[0] = Integer.toString(koppel.getAuction().getId());
        content[1] = Integer.toString(koppel.getProduct().getId());
        connection.update(types, content, query);
    }

    @Override
    public void update(Koppel koppel) {
        String query = "Update Listings SET minimum_bid = ?, is_sold = ? WHERE auction_id = ? AND product_id = ?";
        String[] types = {"float", "boolean", "int", "int"};
        String[] content = new String[4];
        content[0] = Float.toString(koppel.getMinimum_bid());
        content[1] = Boolean.toString(koppel.isIs_sold());
        content[2] = Integer.toString(koppel.getAuction().getId());
        content[3] = Integer.toString(koppel.getProduct().getId());
        connection.update(types, content, query);
    }

    @Override
    public List read(T readstrategy) {
        readstrategy.prepare(connection);
        return readstrategy.execute();
    }
    
}
