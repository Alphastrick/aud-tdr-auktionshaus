package auktionshaus.Persistence;

import auktionshaus.Persistence.Interfaces.IProductAdapter;
import auktionshaus.Persistence.Interfaces.IAuctionAdapter;
import auktionshaus.Persistence.Interfaces.IAdapterFactory;
import auktionshaus.Persistence.Interfaces.IKoppelAdapter;

public class SqlAdapterFactory implements IAdapterFactory{

    private SqlDatabaseWrapper connection;
    public SqlAdapterFactory(String user, String password){
        connection = new SqlDatabaseWrapper(user, password);
    }

    @Override
    public IAuctionAdapter createAuctionAdapter() {
        return new AuctionSqlAdapter(connection);
    }

    @Override
    public IProductAdapter createProductAdapter() {
        return new ProductSqlAdapter(connection);
    }
    
    @Override
    public IKoppelAdapter createKoppelAdapter(){
        return new KoppelSqlAdapter(connection);
    }
    
}
