package auktionshaus.Persistence;

import java.sql.*;
import java.time.LocalDateTime;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.util.Base64;

public class SqlDatabaseWrapper {
    private String user;
    private String password;
    public SqlDatabaseWrapper(String user, String password){
        this.user = user;
        this.password = password;
    }
    
    public void update(String[] type,String[] content, String query){
        
        Connection conn = getConn();
        try {
            PreparedStatement s = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            for (int i = 0; i < type.length;i++){
                switch (type[i]){
                    case "String":
                        s.setString(i+1, content[i]);
                        break;
                    case "int":
                        s.setInt(i+1, Integer.parseInt(content[i]));
                        break;
                    case "float":
                        s.setFloat(i+1,Float.parseFloat(content[i]));
                        break;
                    case "Image":
                        byte[] bytes = Base64.getDecoder().decode(content[i]);
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
                        s.setBlob(i+1, blob);
                        break;
                    case "Date":
                        s.setTimestamp(i+1, Timestamp.valueOf(LocalDateTime.parse(content[i])));
                        break;
                    case "boolean":
                        s.setBoolean(i+1, Boolean.parseBoolean(content[i]));
                        break;
                    default:
                        throw new IllegalArgumentException("no supported type of data");
                    
                }
            }
            s.executeUpdate();
        }
        catch(SQLException e){
            System.err.println("execute" +e.getLocalizedMessage());
            try{
              conn.rollback();  
            }
            catch(SQLException ex){
                System.err.println("Rollback" + ex);
            }
            
        }
        catch(IllegalArgumentException e){
            try{
                System.err.println("no supported type of data");
                conn.rollback();  
            }
            catch(SQLException ex){
                System.err.println("Rollback" + ex);
            }
        }
        deleteConn(conn);

    }
    //returns null if not executed
    public CachedRowSet read(String[] type,String[] content, String query) throws SQLException{
        Connection conn = getConn();
        conn.setAutoCommit(false);
        RowSetFactory factory = RowSetProvider.newFactory();
        CachedRowSet rowSet = factory.createCachedRowSet();
        try{
            PreparedStatement s = conn.prepareStatement(query);
            for (int i = 0; i < type.length;i++){
                switch (type[i]){
                    case "String":
                        s.setString(i+1, content[i]);
                        break;
                    case "int":
                        s.setInt(i+1, Integer.parseInt(content[i]));
                        break;
                    case "float":
                        s.setFloat(i+1,Float.parseFloat(content[i]));
                        break;
                    case "Image":
                        byte[] bytes = Base64.getDecoder().decode(content[i]);
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bytes);
                        s.setBlob(i+1, blob);
                        break;
                }
            }
            rowSet.populate(s.executeQuery());
        }
        catch(SQLException e){
            System.err.println("execute" +e.getLocalizedMessage());
            try{
              conn.rollback();  
            }
            catch(SQLException ex){
                System.err.println("Rollback" + ex);
            }
            
        }
        deleteConn(conn);
        return rowSet;
    }
    //Connect
    private Connection getConn(){
        
        String driver = "org.apache.derby.jdbc.ClientDriver";
        String url = "jdbc:derby://localhost:1527/auktionshaus";
        Connection conn = null;
        
        try {
            Class.forName(driver);
            String usr = this.user;
            String pswd = this.password;
            conn = DriverManager.getConnection(url, usr, pswd);
        } catch (ClassNotFoundException e) {
            System.err.println(e);
        } catch (SQLException e) {
            System.err.println("getConn" + e);
        }
        
        return conn;
        
    }
    private void deleteConn(Connection conn){
        // solange noch kein Pool vorhanden 
        // nicht threadsafe
        try {
            // immer vorsichtshalber commit vor close
            conn.commit();
            // schließen
            conn.close();
            // aufraeumen
            conn = null;
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
}
