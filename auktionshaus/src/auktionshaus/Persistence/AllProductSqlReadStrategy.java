package auktionshaus.Persistence;

import auktionshaus.Persistence.Interfaces.IProductReadStrategy;
import auktionshaus.Product;
import java.util.List;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AllProductSqlReadStrategy implements IProductReadStrategy, ISqlReadStrategy{
  
    private SqlDatabaseWrapper connection;
    
    @Override
    public List<Product> execute() {
        String query = "SELECT image, price_estimation, id, name, description FROM Products";
        String[] types = {};
        String[] content = {};
        CachedRowSet result = null;
        try {
            result = connection.read(types, content, query);
        } catch (SQLException ex) {
            Logger.getLogger(AllProductSqlReadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        List <Product> products = new LinkedList();
        try {
            while(result.next()){
                Product p = new Product();
                
                Blob blob = result.getBlob(1);
                int blobLength = (int) blob.length();
                byte[] blobAsBytes = blob.getBytes(1, blobLength);
                p.setImage(blobAsBytes);
                
                p.setPriceEstimation(result.getFloat(2));
                p.setId(result.getInt(3));
                p.setName(result.getString(4));
                p.setDescription(result.getString(5));
                products.add(p);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AllProductSqlReadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void prepare(SqlDatabaseWrapper connection) {
        this.connection = connection;
    }
    
}
