package auktionshaus.Persistence;

import auktionshaus.Auction;
import auktionshaus.Koppel;
import auktionshaus.Persistence.Interfaces.IKoppelReadStrategy;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import auktionshaus.Product;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.LinkedList;

public class KoppelSqlReadStrategy_ByAuction implements IKoppelReadStrategy, ISqlReadStrategy{

    private SqlDatabaseWrapper connection;
    private int auction_id;

    public KoppelSqlReadStrategy_ByAuction(int auction_id) {
        this.auction_id = auction_id;
    }
    
    @Override
    public List<Koppel> execute() {
        String query = "SELECT auction_id, product_id, minimum_bid, is_sold FROM Listings WHERE auction_id  = ?";
        String[] types = {"int"};
        String[] content = new String[1];
        content[0] = Integer.toString(auction_id);
        CachedRowSet result = null;
        try {
            result = connection.read(types, content, query);
        } catch (SQLException ex) {
            Logger.getLogger(KoppelSqlReadStrategy_ByAuction.class.getName()).log(Level.SEVERE, null, ex);
        }
        List <Koppel> koppel = new LinkedList();
        try {
            while(result.next()){
                Koppel k = new Koppel();
                
                //Populating the whole auction and product
                SqlAdapterFactory factory = new SqlAdapterFactory("auktionshaus_user", "1234");
                AuctionSqlAdapter auction_adapter = (AuctionSqlAdapter) factory.createAuctionAdapter();
                SqlReadStrategyFactory readfactory = new SqlReadStrategyFactory();
                Auction a = (Auction) auction_adapter.read((ISqlReadStrategy) readfactory.createSingleAuctionReadStrategy(result.getInt(1))).get(0);
                k.setAuction(a);
                
                ProductSqlAdapter product_adapter = (ProductSqlAdapter) factory.createProductAdapter();
                Product p = (Product) product_adapter.read((ISqlReadStrategy) readfactory.createSingleProductReadStrategy(result.getInt(2))).get(0); 
                k.setProduct(p);
                
                k.setMinimum_bid(result.getFloat(3));
                k.setIs_sold(result.getBoolean(4));
                koppel.add(k);
            }
        } catch (SQLException ex) {
            Logger.getLogger(KoppelSqlReadStrategy_ByAuction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return koppel;
    }

    @Override
    public void prepare(SqlDatabaseWrapper connection) {
        this.connection = connection;
    }
    
}
