package auktionshaus.Persistence.Interfaces;

import auktionshaus.Persistence.SqlDatabaseWrapper;

public interface ISqlReadStrategy {
    void prepare(SqlDatabaseWrapper connection);
}
