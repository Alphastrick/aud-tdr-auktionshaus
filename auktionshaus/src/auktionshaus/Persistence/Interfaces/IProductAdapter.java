package auktionshaus.Persistence.Interfaces;

import auktionshaus.Product;
import java.util.List;

public interface IProductAdapter <T extends IProductReadStrategy>{
    void insert(Product product);
    void delete (Product product);
    void update (Product product);
    List<Product> read(T readstrategy);
}
