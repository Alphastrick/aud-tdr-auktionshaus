package auktionshaus.Persistence.Interfaces;

import auktionshaus.Auction;
import auktionshaus.Koppel;
import auktionshaus.Product;
import java.util.List;

public interface IKoppelAdapter <T extends IKoppelReadStrategy>{
    void insert(Koppel koppel);
    void delete(Koppel koppel);
    void update(Koppel koppel);
    List<Koppel> read(T readstrategy);
}
