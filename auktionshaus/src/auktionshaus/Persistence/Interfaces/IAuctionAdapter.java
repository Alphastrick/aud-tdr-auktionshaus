package auktionshaus.Persistence.Interfaces;

import auktionshaus.Auction;
import java.util.List;

public interface IAuctionAdapter <T extends IAuctionReadStrategy>{
    void insert(Auction auction);
    void delete(Auction auction);
    void update(Auction auction);
    List<Auction> read(T readstrategy);
}
