package auktionshaus.Persistence.Interfaces;

public interface IAdapterFactory {
    IAuctionAdapter createAuctionAdapter();
    IProductAdapter createProductAdapter();
    IKoppelAdapter createKoppelAdapter();
}
