package auktionshaus.Persistence.Interfaces;

import auktionshaus.Koppel;
import java.util.List;

public interface IKoppelReadStrategy{
    List <Koppel> execute();
}
