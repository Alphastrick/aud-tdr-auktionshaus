package auktionshaus.Persistence.Interfaces;

public interface IReadStrategyFactory {
    IProductReadStrategy createSingleProductReadStrategy(int id);
    IAuctionReadStrategy createSingleAuctionReadStrategy(int id);
    public IKoppelReadStrategy createKoppelReadStrategy_ByAuction(int auction_id);
    public IKoppelReadStrategy createKoppelReadStrategy_ByProduct(int product_id);
    public IProductReadStrategy createAllProductReadStrategy();
    public IAuctionReadStrategy createAllAuctionReadStrategy();
}
