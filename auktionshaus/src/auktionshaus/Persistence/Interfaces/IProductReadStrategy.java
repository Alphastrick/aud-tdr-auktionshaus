package auktionshaus.Persistence.Interfaces;

import auktionshaus.Product;
import java.util.List;

public interface IProductReadStrategy {
    List<Product> execute();
}
