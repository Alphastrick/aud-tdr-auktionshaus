package auktionshaus.Persistence.Interfaces;

import auktionshaus.Auction;
import java.util.List;

public interface IAuctionReadStrategy {
    List<Auction> execute();
}
