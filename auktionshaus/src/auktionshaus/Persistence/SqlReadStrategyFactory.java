package auktionshaus.Persistence;

import auktionshaus.Persistence.Interfaces.IAuctionReadStrategy;
import auktionshaus.Persistence.Interfaces.IKoppelReadStrategy;
import auktionshaus.Persistence.Interfaces.IProductReadStrategy;
import auktionshaus.Persistence.Interfaces.IReadStrategyFactory;

public class SqlReadStrategyFactory implements IReadStrategyFactory{

    @Override
    public IProductReadStrategy createSingleProductReadStrategy(int id) {
        return new SingleProductSqlReadStrategy(id);
    }

    @Override
    public IAuctionReadStrategy createSingleAuctionReadStrategy(int id) {
        return new SingleAuctionSqlReadStrategy(id);
    }
    
    @Override
    public IKoppelReadStrategy createKoppelReadStrategy_ByAuction(int auction_id){
        return new KoppelSqlReadStrategy_ByAuction(auction_id);
    }
    
    @Override
    public IKoppelReadStrategy createKoppelReadStrategy_ByProduct(int product_id){
        return new KoppelSqlReadStrategy_ByAuction(product_id);
    }
    
    public IProductReadStrategy createAllProductReadStrategy(){
        return new AllProductSqlReadStrategy();
    }
    
    public IAuctionReadStrategy createAllAuctionReadStrategy(){
        return new AllAuctionSqlReadStrategy();
    }
    
}
