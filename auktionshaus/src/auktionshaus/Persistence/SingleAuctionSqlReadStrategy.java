package auktionshaus.Persistence;

import auktionshaus.Auction;
import auktionshaus.Persistence.Interfaces.IAuctionReadStrategy;
import java.util.List;
import auktionshaus.Persistence.Interfaces.ISqlReadStrategy;
import java.sql.*;
import javax.sql.rowset.CachedRowSet;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Read a single Auction by the given auction ID
 */
public class SingleAuctionSqlReadStrategy implements IAuctionReadStrategy, ISqlReadStrategy{

    private SqlDatabaseWrapper connection;
    private int id;
    
    public SingleAuctionSqlReadStrategy(int id) {
        this.id = id;
    }

    public List<Auction> execute() {
        String query = "SELECT scheduled_date, scheduled_release, id, name, description FROM Auctions WHERE id = ?";
        String[] types = {"int"};
        String[] content = new String[1];
        content[0] = String.valueOf(id);
        CachedRowSet result = null;
        try {
            result = connection.read(types, content, query);
        } catch (SQLException ex) {
            Logger.getLogger(SingleAuctionSqlReadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        List <Auction> auctions = new LinkedList();
        try {
            while(result.next()){
                Auction a = new Auction();
                a.setScheduledDate(result.getTimestamp(1).toLocalDateTime());
                a.setReleaseDate(result.getTimestamp(2).toLocalDateTime());
                a.setId(result.getInt(3));
                a.setName(result.getString(4));
                a.setDescription(result.getString(5));
                auctions.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SingleAuctionSqlReadStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return auctions;
    }

    @Override
    public void prepare(SqlDatabaseWrapper connection) {
        this.connection = connection;
    }
    
}
